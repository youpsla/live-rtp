# coding: utf8
#!/usr/bin/env conda run -n livertp python

import os
import sys
import asyncio
import tweepy


# Necessary for importing from models
path = os.path.join(os.path.dirname(__file__), os.pardir)
sys.path.append(path)

from commons.models import Daily

API_KEY = 'bOs1gOG9kFXuJd8KZLDWGiXtj'
API_SECRET = 'V1dnwCKCLMEf2YhYGLZ394ZgXRI0zuuOgS5DgJlTBzORxXvRUD'
ACCESS_TOKEN = '1283491059083948037-AfLTWq0qoKUtC36c7AN1qpGl2U7uIP'
ACCESS_TOKEN_SECRET = 'kH7kUMJOpsOh0WHKSlfHAy12SZNbZM8ToRNcXyc2d5Iin'



# Authenticate to Twitter
auth = tweepy.OAuthHandler(API_KEY, API_SECRET)
auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)

# Create API object
api = tweepy.API(auth)


async def create_tweet():
    datas = await Daily().largest_moves(hours=3)
    text = ''
    cpt = 0
    for row in datas:
        line = f"{row['name']} {row['delta']} {row['start_rtp']} {row['end_rtp']}\n"
        if len(text) < 241 - len(line):
            cpt += 1
            text += line
        else:
            break
    
    header = f'Top {cpt} daily rtp raises in last hour:\n\n'

    status = header + text

    api.update_status(status)

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(create_tweet())
