#!/usr/bin/env conda run -n livertp python


from scrapper.games_scrapper import parse
import asyncio
import sys



if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    while True:
        try:
            loop.run_until_complete(parse())
        except Exception as e:
            if e == KeyboardInterrupt:
                print('Interrupt by user')
                sys.exit()
            else:
                print('Error but restart')
