# coding: utf8
#!/usr/bin/env conda run -n livertp python

# TODO: Create class Parser.

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import (
    StaleElementReferenceException,
    TimeoutException,
)

from time import sleep
from lxml import html
from lxml import etree
import asyncio
import inspect
from multiprocessing import Pool

import time

import argparse, sys
import os

import undetected_chromedriver as uc

path = os.path.join(os.path.dirname(__file__), os.pardir)
sys.path.append(path)

from commons.models import (
    Casino,
    write_rtps_2,
    create_stats_view,
)
from commons.logger import logger

SORTS = {
    "rtpLivePastDay": "Last day",
    "rtpLivePastSevenDays": "Last 7 days",
    "rtpLivePastThirtyDays": "Last 30 days",
}

def get_driver(mode="headless"):
    """Start web driver"""
    options = uc.ChromeOptions()
    options.add_argument("--blink-settings=imagesEnabled=false")
    options.add_argument("--window-size=1000,1200")
    mode = 'dede'
    if mode == "headless":
        options.add_argument("--headless")
        options.add_argument("--disable-gpu")
    driver = uc.Chrome(use_subprocess=True, options=options)
    sleep(2)
    logger.info(f"Chrome driver with id {id(driver)} initiated")
    return driver

class CasinoScrapper:
    def __init__(self, casino_name="", testing=True):
        self.casino_name = casino_name
        self.testing = testing
        self.driver = get_driver()
        self.records = []

    async def _init(self):
        self.casino = await Casino.get_by_name(self.casino_name)

    def get_slots(self):
        page = html.document_fromstring(self.driver.page_source)
        tree = etree.ElementTree(page)
        slots = tree.findall(self.casino.xpaths["slot"])
        try:
            logger.info(f"Slots found: {len(slots)}")
        except:
            pass
        return slots

    def get_slot_name(self, slot):
        path = self.casino.xpaths["slot_name"]
        name = str(slot.find(path).get("href")).rsplit("/", 1)[-1]
        return name

    def get_slot_screen_name(self, slot):
        path = self.casino.xpaths["slot_screen_name"]
        name = slot.find(path).text_content()
        if not name:
            logger.info(f'{self.casino_name} - {inspect.currentframe().f_code.co_name} returns None')
        return name

    def get_slot_provider_screen_name(self, slot):
        path = self.casino.xpaths["provider_screen_name"]
        name = slot.find(path).text_content()

        if not name:
            logger.info(f'{self.casino_name} - {inspect.currentframe().f_code.co_name} returns None')
        return name

    def get_slot_rtp(self, slot):
        path = self.casino.xpaths["slot_rtp"]
        rtp = slot.find(path).text_content()[0:-1]
        return rtp

    def get_slot_provider_name(self, slot):
        path = self.casino.xpaths["provider_name"]
        if self.casino.xpaths['scraping_type'] == 1:
            return None
        name = str(slot.find(path).get("href")).rsplit("/", 1)[-1]
        if not name:
            logger.info(f'{self.casino_name} - {inspect.currentframe().f_code.co_name} returns None')
        return name

    def goto_slots_page(self):
        self.driver.get(self.casino.url_rtp)
        time.sleep(2)
        try:
            category_dropdown = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located(
                    (
                        By.XPATH,
                        self.casino.xpaths["category_dropdown"],
                    )
                )
            )
        except TimeoutException as e:
            print(f"Categry dropdown not found: {e}")
        self.driver.execute_script("arguments[0].click();", category_dropdown)

        checkbox_slots = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(
                (
                    By.XPATH,
                    self.casino.xpaths["checkbox_slots"],
                )
            )
        )
        self.driver.execute_script("arguments[0].click();", checkbox_slots)

    def get_more_button(self):
        print(self.casino.xpaths["more_button"])
        more_button = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(
                (
                    By.XPATH,
                    self.casino.xpaths["more_button"],
                )
            )
        )
        return more_button

    async def collect_from_website(self):
        self.goto_slots_page()
        more_button = self.get_more_button()

        cpt = 0
        while True:
            try:
                self.driver.execute_script("arguments[0].click();", more_button)
                cpt += 1
                logger.info(f"More button clicked on page {cpt}")
                sleep(0.5)
                if self.testing == True:
                    break
            except TimeoutException:
                logger.info(f"Timeout to find more_button for page {cpt}")
            except StaleElementReferenceException:
                break

        slots = self.get_slots()
        if len(slots) > 0:
            for slot in slots:
                try:
                    record = {
                        "slot_screen_name": self.get_slot_screen_name(slot),
                        "slot_name": self.get_slot_name(slot),
                        "rtp": self.get_slot_rtp(slot),
                        "provider_name": self.get_slot_provider_name(slot),
                        "provider_screen_name": self.get_slot_provider_screen_name(
                            slot
                        ),
                    }
                    self.records.append(record)
                except Exception as e:
                    logger.info(
                        # f"Error while extracting slot info from HTML: {e} - {etree.tostring(slot)}"
                        f"Error while extracting slot info from HTML: {e}"
                    )
        else:
            logger.info("No slots found")

        # Check if all scrapped providers already exists in DB. If not, create them.
        scrapped_providers = {
            r["provider_name"]: r["provider_screen_name"] for r in self.records
        }
        await self.casino.create_new_providers(scrapped_providers)

        # Check if all scrapped slots already exists in DB. If not, create them.
        scrapped_slots = {
            r["slot_name"]
            + "_"
            + r["provider_name"]
            + "_"
            + self.casino.name: (
                r["slot_name"],
                r["slot_screen_name"],
                r["provider_name"],
            )
            for r in self.records
        }
        await self.casino.create_new_slots(scrapped_slots)

        if len(self.records) > 0:
            data = dict(records=self.records, sort="rtpLivePastDay", casino=self.casino)
            logger.info(f"{len(self.records)} found")
            await write_rtps_2(self.casino, data)
            self.records = []
        else:
            logger.info("No records found in webpage.")

    async def launch(self):
        d_counter = 0
        w_counter = 0

        while True:
            d_counter += 1
            sort = "rtpLivePastDay"
            view = "daily"
            # if d_counter == 3:
            #     sort = "rtpLivePastSevenDays"
            #     view = "weekly"
            #     w_counter += 1
            #     d_counter = 0
            # if w_counter == 4:
            #     sort = "rtpLivePastThirtyDays"
            #     view = "monthly"
            #     w_counter = 0
            await self.collect_from_website()
            sleep(10)
            await create_stats_view(view)



def get_casinos():
    loop = asyncio.get_event_loop()
    return loop.run_until_complete(Casino.get_all())

async def create_casino_scrapper(**kwargs):
    cs = CasinoScrapper(**kwargs)
    print(f'CS: {cs}')
    await cs._init()
    await cs.launch()

def launch_casino_scrapper(kwargs):
    loop = asyncio.new_event_loop()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(create_casino_scrapper(**kwargs))

def main():
    pool = Pool()
    casinos = get_casinos()
    for casino in casinos:
        logger.info(casino.name)
        # if casino.name == 'sportsbet':
        if casino.name == 'bitcasino':
            pool.apply_async(launch_casino_scrapper, [{'casino_name': casino.name}])
    pool.close()
    pool.join()

if __name__ == "__main__":
    # parser = argparse.ArgumentParser()
    # parser.add_argument("--mode", help="If set to headless, then run in headless mode")
    # parser.add_argument("--testing", help="If set to true, only first page is parsed")
    # args = parser.parse_args()

    # mode = args.mode
    # testing = True if args.testing else False
    # if mode:
    #     logger.info(f"Parse script launched with mode={mode} and testing={testing}")

    # loop = asyncio.get_event_loop()
    # sd = Scrapper.driver
    # cs1 = loop.run_until_complete(
    #     create_casino_scrapper(casino_name="bitcasino", driver=sd, testing=False)
    # )
    # loop.run_until_complete(cs1.launch())

    # from multiprocessing import Pool

    # lst = [{"casino_name": "bitcasino"}, {"casino_name": "bitcasino"}]

    # pool = Pool()

    # for i in lst:
    #     pool.apply_async(launch, [i])

    # pool.close()
    # pool.join()
    main()
