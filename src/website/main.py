# coding: utf8
#!/usr/bin/env conda run -n rtp python

# TODO: Add user management
# TODO: Add link on most played for updating chart

import enaml
from tornado.web import Application, RequestHandler, StaticFileHandler
from tornado.websocket import WebSocketHandler
from tornado.ioloop import IOLoop
from tornado.web import app_log
from tornado import autoreload
from web.core.app import WebApplication
from aiopg.sa import create_engine
import json
import asyncio

from tornado.platform.asyncio import to_tornado_future

import sys
import os

path = os.path.join(os.path.dirname(__file__), os.pardir)
sys.path.append(path)

from atomdb.sql import SQLModelManager
from commons.models import DB_PARAMS, Provider, Slot, Daily, reset_tables
from sqlalchemy import and_
import sqlalchemy as sa

import logging
from logging.handlers import RotatingFileHandler
from tornado.log import LogFormatter

# Define logger
logger = logging.getLogger('tornado.access')
logger.setLevel(logging.DEBUG)
formatter = LogFormatter(color=False, datefmt=None)
file_handler = RotatingFileHandler('log/rtp.log', 'a', 1000000, 1)
file_handler.setFormatter(formatter)
# file_handler.setLevel(logging.DEBUG)
logger.addHandler(file_handler)

with enaml.imports():
    from index import Index

# Holds the rendered view so a websocket can retrieve it later
CACHE = {}


class IndexHandler(RequestHandler):
    async def get(self):
        providers = await Provider().get_active()
        most_played = await Daily().most_played()
        largest_moves_up = await Daily().largest_moves()
        largest_moves_down = await Daily().largest_moves(direction='<')

        index = Index(providers=providers,
                      most_played=most_played,
                      largest_moves_up=largest_moves_up,
                      largest_moves_down=largest_moves_down)

        # Store the viewer in the cache
        CACHE[index.id] = index

        response = index.render()
        response = "<!DOCTYPE html>\n" + response

        self.write(response)


class WsHandler(WebSocketHandler):
    clients = []
    index = None

    def check_origin(self, origin):
        return True

    async def open(self, *args, **kwargs):
        # Store the viewer in the cache
        self.clients.append((self))

        # Lookup the viewer in the cache
        ref = self.get_argument("id")
        if ref not in CACHE:
            logger.error(f"Index with ref={ref} does not exist!")
            self.close()
            return

        # Get a viewer reference
        self.index = CACHE[ref]

        # Setup an observer to watch changes on the enaml view
        self.index.observe('modified', self.on_dom_modified)

    async def on_message(self, message):

        change = json.loads(message)
        # log.debug(f'Update from js: {change}')

        # Lookup the node
        ref = change.get('id')
        if not ref:
            return
        nodes = self.index.xpath('//*[@id=$ref]', ref=ref)
        if not nodes:
            return  # Unknown node
        node = nodes[0]

        # Trigger the change on the enaml node
        if change.get('type') and change.get('name'):
            # print(change)
            if change['type'] == 'event':
                trigger = getattr(node, change['name'])
                print(f'Change name: {change["name"]}')
                print(f'Trigger: {trigger}')
                trigger()
            elif change['type'] == 'update':
                # Trigger the update
                setattr(node, change['name'], change['value'])
        else:
            pass
            # log.warning(f"Unhandled event {self} {node}: {change}")

    def on_dom_modified(self, change):
        """ When an event from enaml occurs, send it out the websocket
        so the client's browser can update accordingly.

        """
        self.write_message(json.dumps(change['value']))


def main():
    enaml_app = WebApplication()
    app = Application([
        (r"/", IndexHandler),
        (r"/static/(.*)", StaticFileHandler, {
            "path": "static"
        }),
        (r"/websocket/", WsHandler),
    ], )

    app.listen(7777)
    #TODO remove in prod
    autoreload.start()
    autoreload.watch('main.py')
    autoreload.watch('index.enaml')

    IOLoop.current().start()


if __name__ == "__main__":
    print('Restart tornado server')
    loop = asyncio.get_event_loop()
    main()
