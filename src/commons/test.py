import asyncio
import os
from atomdb.sql import SQLModel, SQLModelManager, Relation, find_sql_models
from atom.api import (
    Int,
    Instance,
)

from aiopg.sa import create_engine
from sqlalchemy.schema import DropTable
from sqlalchemy.ext.compiler import compiles

DB_PARAMS = dict(
    host=os.environ["DB_HOST"],
    port=os.environ["DB_PORT"],
    user=os.environ["DB_USER"],
    password=os.environ["DB_USER_PASSWORD"],
    database=os.environ["DB"],
)

# Update drop_table for postges
@compiles(DropTable, "postgresql")
def _compile_drop_table(element, compiler, **kwargs):
    return compiler.visit_drop_table(element) + " CASCADE"


class Mother(SQLModel):
    age = Int()
    childrens = Relation(lambda: Child)

class Child(SQLModel):
    age = Int()
    mother = Instance(Mother).tag(nullable=False)


async def reset_tables():
    async with create_engine(**DB_PARAMS, echo=True) as engine:
        mgr = SQLModelManager.instance()
        mgr.database = engine
        mgr.create_tables()
        for Model in find_sql_models():
            try:
                await Model.objects.drop_table()
            except Exception as e:
                msg = str(e)
                if not ("Unknown table" in msg or "does not exist" in msg):
                    raise  # Unexpected error
            await Model.objects.create_table()

async def create_datas():
    async with create_engine(**DB_PARAMS, echo=True) as engine:
        mgr = SQLModelManager.instance()
        mgr.database = engine
        mother = await Mother.objects.create(age=40)
        await Child.objects.create(mother=mother, age=15)
        await Child.objects.create(mother=mother, age=12)

async def test_select_related(id=1):
    async with create_engine(**DB_PARAMS, echo=True) as engine:
        mgr = SQLModelManager.instance()
        mgr.database = engine
        # return await Mother.objects.select_related('childrens').filter(_id=1).all()
        return await Mother.objects.select_related('childrens').filter(_id=1).all()
        # return await Mother.objects.prefetch_related('childrens').filter(_id=1).all()

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(reset_tables())
    loop.run_until_complete(create_datas())


    mother = loop.run_until_complete(test_select_related())
    print(mother[0].childrens)