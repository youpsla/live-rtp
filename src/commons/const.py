CASINOS = [
    {
        "name": "bitcasino",
        "screen_name": "Bitcasino",
        "url": "https://bitcasino.io",
        "url_rtp": "https://bitcasino.io/live-rtp",
        "xpaths": {
            "scraping_type": 0,
            "category_dropdown": '//*[@id="__next"]/div/div[3]/div/div[2]/div/div/div[3]/div/div[1]/div/div[1]/div/button',
            "checkbox_slots": '//*[@id="__next"]/div/div[3]/div/div[2]/div/div/div[3]/div/div[1]/div/div[1]/div/div/div/div/ul/div[1]/label/label/input',
            "more_button": '//*[@id="__next"]/div/div[3]/div/div[2]/div/div/div[3]/div/button',
            "slot": '//*[@id="__next"]/div/div[3]/div/div[2]/div/div/div[3]/div/div[2]/div',
            "slot_name": "div[2]/a/div/div[1]",
            "slot_screen_name": "div[2]/a/div/div[1]/p",
            "provider_name": "div[2]/a/div/div[2]",
            "provider_screen_name": "div[2]/a/div/div[2]/p",
            "slot_rtp": "div[3]/div/span",
        },
    },
    {
        "name": "sportsbet",
        "screen_name": "Sportsbet",
        "url": "https://sportsbet.io",
        "url_rtp": "https://sportsbet.io/casino/live-rtp",
        "xpaths": {
            "scraping_type": 1,
            "category_dropdown": '//*[@id="root"]/div/div[1]/div[2]/div[2]/div/div[1]/div/div[2]/div[1]/button', # ok
            "checkbox_slots": '//*[@id="video-slots"]',# ok
            "more_button": '//*[@id="root"]/div/div[1]/div[2]/div[2]/div/section/div[2]/button', # ok
            "slot": '//*[@id="root"]/div/div[1]/div[2]/div[2]/div/section/div[1]/div', # ok
            "slot_name": "a", # ok
            "slot_screen_name": "a/div[1]/div[2]/p", # ok
            "provider_name": "",
            "provider_screen_name": "a/div[1]/div[2]/span", # ok
            "slot_rtp": "a/div[2]/span[1]",
        },
    },
    {
        "name": "slots",
        "screen_name": "Slots",
        "url": "https://slots.io",
        "url_rtp": "https://slots.io",
        "xpaths": {
            "scraping_type": 1,
            "category_dropdown": '//*[@id="__next"]/div/div[2]/div/div[2]/div/div/div[3]/div/div[1]/div/div[1]/div/button',
            "checkbox_slots": '//*[@id="__next"]/div/div[2]/div/div[2]/div/div/div[3]/div/div[1]/div/div[1]/div/div/div/div/ul/div[1]/label/label/input',
            "more_button": '//*[@id="__next"]/div/div[2]/div/div[2]/div/div/div[3]/div/button',
            "slot": '//*[@id="__next"]/div/div[2]/div/div[2]/div/div/div[3]/div/div[2]',
            "slot_name": "div[2]/a/div/div[1]",
            "slot_screen_name": "div[2]/a/div/div[1]/p",
            "provider_name": "div[2]/a/div/div[2]",
            "provider_screen_name": "div[2]/a/div/div[2]/p",
        },
    },
]